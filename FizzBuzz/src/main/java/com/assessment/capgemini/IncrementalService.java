package com.assessment.capgemini;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

@Path("/increment")
public class IncrementalService {

	private static int i;
	
	@GET
	@Path("/count")
	//@Consumes("application/x-www-form-urlencoded")
	public Response getNumberCount(@QueryParam("number") String number,
			@QueryParam("next") String next,
			@QueryParam("previous") String previous) {

		Date now = new Date();
		 
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("E");
        System.out.println(simpleDateformat.format(now));
        
		i = Integer.valueOf(number);
		String output = number;
		
		if(i > 0 && i<1000){
			//start processing
			if(i<=20 && next == null && previous == null){
				if(i%3==0 && i%5==0){
					if(simpleDateformat.format(now).equals("Wed")){
						System.out.println("wizz wuzz");
					}
					System.out.println("fizz buzz");
				}else if(i%3==0){
					if(simpleDateformat.format(now).equals("Wed")){
						System.out.println("wizz");
					}
					System.out.println("fizz");
				}else if(i%5==0){
					if(simpleDateformat.format(now).equals("Wed")){
						System.out.println("wuzz");
					}
					System.out.println("buzz");
				}else{
					System.out.println(number);
				}
			}else{
				output = "Please add query parameter 'Next' in the URL to see next values";
			}
		}else{
			output = "Number should be positive and between 1 and 1000";
			return Response.status(202).entity(output).build();
		}
		
		if(i > 0 && i<1000){
			if(next != null && i>20){
				if(i%3==0 && i%5==0){
					if(simpleDateformat.format(now).equals("Wed")){
						System.out.println("wizz wuzz");
					}
					System.out.println("fizz buzz");
				}else if(i%3==0){
					if(simpleDateformat.format(now).equals("Wed")){
						System.out.println("wizz");
					}
					System.out.println("fizz");
				}else if(i%5==0){
					if(simpleDateformat.format(now).equals("Wed")){
						System.out.println("wuzz");
					}
					System.out.println("buzz");
				}else{
					System.out.println(number);
				}
			}else if(previous != null && i>20){
				if(i%3==0 && i%5==0){
					if(simpleDateformat.format(now).equals("Wed")){
						System.out.println("wizz wuzz");
					}
					System.out.println("fizz buzz");
				}else if(i%3==0){
					if(simpleDateformat.format(now).equals("Wed")){
						System.out.println("wizz");
					}
					System.out.println("fizz");
				}else if(i%5==0){
					if(simpleDateformat.format(now).equals("Wed")){
						System.out.println("wuzz");
					}
					System.out.println("buzz");
				}else{
					System.out.println(number);
				}
			}
			else{
				output = "Bad URL/Input";
				return Response.status(202).entity(output).build();
			}
		}else{
			output = "Number should be positive and between 1 and 1000";
			return Response.status(202).entity(output).build();
		}
		
		
		return Response.status(200).entity(output).build();

	}
	
}